const {
  dialog,
  session,
  app,
  BrowserWindow,
  ipcMain
} = require("electron");
const path = require("path");
let mainWindow;
const urlfrmt = require("url");
const isDev = require("electron-is-dev");
var ps = require('ps-node');
var spawn = require('child_process').spawn;
var Registry = require('winreg');

const filter = {
  urls: ['https://*.steampowered.com/*']
}
const filter2 = {
  urls: ['https://steamstore-a.akamaihd.net/public/shared/javascript/*']
}

function createWindow() {
  mainWindow = new BrowserWindow({
    show: true,
    width: 800,
    show: false,
    height: 600,
    webPreferences: {
      webSecurity: false,
      nodeIntegration: false,
      backgroundColor: '#272B30',
      preload: path.join(__dirname, 'preload/preload.js')
    }
  })
  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  });
  mainWindow.loadURL("http://accgen.cathook.club/");

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

ipcMain.once("accgen-web-ready", (event, arg) => {
  if (!isDev)
    require("./updater").default(app, event);
  console.log("Ready got!");
});

function stopSteam() {
  return new Promise(function (resolve) {
    ps.lookup({
      command: 'steam'
    }, function (err, resultList) {
      if (err) {
        throw new Error(err);
      }
      resultList = resultList.filter(function (entry) {
        var command = entry.command.toLowerCase()
        return command.endsWith("steam") || command.endsWith("steam.exe")
      });

      if (resultList.length > 0) {
        // Check if user allows us to kill other steams
        dialog.showMessageBox(null, {
          type: 'question',
          buttons: ['Yes', 'No'],
          defaultId: 2,
          title: 'Kill steam?',
          message: 'Steam is already running. Would you like to terminate steam and continue?'
        }, (response) => {
          console.log(response)
          var promises = [];
          if (response == 0) {
            for (var i = 0; i < resultList.length; i++) {
              if (resultList[i] && resultList[i].pid) {
                var pid = resultList[i].pid;
                promises.push(new Promise(function (resolve, reject) {
                  ps.kill(pid, {
                    timeout: 10
                  }, function (err) {
                    if (err) {
                      console.error(err);
                      resolve();
                    } else {
                      console.log('Process %s has been killed!', pid);
                      resolve();
                    }
                  });
                }));
              }
            }
          }
          Promise.all(promises).then(resolve);
        });
      } else
        resolve();
    });
  })
}

async function startSteam(account) {
  var path;
  if (process.platform == "win32") {
    regKey = new Registry({
      hive: Registry.HKCU,
      key: '\\Software\\Valve\\Steam\\'
    })
    await new Promise(function (resolve) {
      regKey.values(function (err, items) {
        if (err)
          path = err;
        else
          for (var i = 0; i < items.length; i++)
            if (items[i].name == "SteamExe") {
              path = items[i].value;
              resolve();
              break;
            }
        resolve();
      });
    })
  } else
    path = "steam";
  if (path instanceof Error || !path) {
    dialog.showMessageBox(null, {
      type: 'error',
      title: 'Failed to start steam',
      message: 'SAG Electron was not able to find the steam executable.',
      buttons: ['Ok']
    });
    return;
  }
  if (!account || !account.login || account.login.includes(" ") || account.login.includes("-") || !account.password || account.password.includes(" ") || account.password.includes("-")) {
    dialog.showMessageBox(null, {
      type: 'error',
      title: 'RCE Stopped',
      message: 'SAG Electron stopped a potential RCE attempt. (Or the login information returned by the website was empty)',
      buttons: ['Ok']
    });
    return;
  }
  var steam = spawn(path, `-login ${account.login} ${account.password}`.split(" "), {
    detached: true,
    stdio: 'ignore'
  });
  steam.on('error', function (err) {
    if (err.errno == "ENOENT")
      dialog.showMessageBox(null, {
        type: 'error',
        title: 'Failed to start steam',
        message: 'SAG Electron was not able to find the steam executable.',
        buttons: ['Ok']
      });
    console.error(err);
  });
}

ipcMain.on("start-steam", async (event, arg) => {
  await stopSteam();
  console.log(arg);
  startSteam(arg);
});

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('browser-window-created', function (e, window) {
  window.setMenuBarVisibility(false);
});

app.on('ready', function () {
  createWindow();

  session.defaultSession.webRequest.onBeforeSendHeaders(filter, (details, callback) => {
    details.requestHeaders['Origin'] = 'file://'
    callback({
      cancel: false,
      requestHeaders: details.requestHeaders
    })
  });

  session.defaultSession.webRequest.onHeadersReceived(filter, (details, callback) => {
    delete details.responseHeaders['X-Frame-Options'];
    details.responseHeaders['Access-Control-Allow-Origin'] = '*'
    delete details.responseHeaders['Content-Security-Policy'];
    callback({
      cancel: false,
      responseHeaders: details.responseHeaders
    })
  });

  session.defaultSession.webRequest.onBeforeRequest(filter2, (details, callback) => {
    const {
      url
    } = details;
    const localURL = url.replace("https://steamstore-a.akamaihd.net/public/shared/javascript/jquery-1.8.3.min.js", urlfrmt.format({
      pathname: path.join(__dirname, 'injected.js'),
      protocol: 'file:',
      slashes: true
    }))
    callback({
      cancel: false,
      redirectURL: ((localURL))
    });
  });
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})